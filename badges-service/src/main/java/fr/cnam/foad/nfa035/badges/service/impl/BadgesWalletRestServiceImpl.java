package fr.cnam.foad.nfa035.badges.service.impl;

import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URI;
import java.util.Set;

@RestController
public class BadgesWalletRestServiceImpl implements BadgesWalletRestService {

    JSONBadgeWalletDAO jsonBadgeDao;

    public void deleteBadge() throws IOException {

    }



    public void putBadge () {

    }

    public void readBadge() {

    }


    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException {
        jsonBadgeDao = new JSONBadgeWalletDAOImpl("badges-wallet/src/test/resources/" + "db_wallet.json");
        return ResponseEntity
                .ok()
                .body(jsonBadgeDao.getWalletMetadata());
    }
}
