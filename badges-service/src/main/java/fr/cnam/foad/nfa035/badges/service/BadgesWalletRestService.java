package fr.cnam.foad.nfa035.badges.service;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/_badges")

public interface BadgesWalletRestService {

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère les métadonnées du Wallet",
            description = "récupère les métadonnées du portefeuille, donc l'index des badges qui s'y trouve")
    @Tag(name = "getMetadata")
    @GetMapping("/metas")
    ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException;


}
