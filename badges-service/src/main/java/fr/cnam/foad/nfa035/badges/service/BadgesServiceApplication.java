package fr.cnam.foad.nfa035.badges.service;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(
        title = "Service d'accès au portefeuille de Badge",
        version = "1.0.0-SNAPSHOT",
        description = "API permettant la manipulation d'entités de type DigitalBadge au sein de notre portefeuille au format JSON(wallet)"+
           "<br/> Il s'agit simplement en termes d'opérations, de répondre aux exigences CRUD (Create, Read, Update, Delete)",
        license = @License(name = "Tous droits réservés", url = "https://lecnam.net"),
        contact = @Contact(name = "<BOUDARD-TERRIEN Laura", email = "<laura.boudard-terrien.auditeur@lecnam.net")
))

@SpringBootApplication
public class BadgesServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(BadgesServiceApplication.class, args);
    }
}
